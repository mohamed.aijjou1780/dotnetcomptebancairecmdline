﻿using System;
using System.Collections.Generic;
using System.Text;
using MySql.Data.MySqlClient;

namespace GestionCompteBancaire.Classes
{
    class Client
    {
        private long id;
        private string nom;
        private string prenom;
        private string telephone;
        List<Compte> comptes;

        public string Nom { get => nom; set => nom = value; }
        public string Prenom { get => prenom; set => prenom = value; }
        public string Telephone { get => telephone; set => telephone = value; }
        public long Id { get => id; set => id = value; }

        public Client(string n, string p, string t)
        {
            Nom = n;
            Prenom = p;
            Telephone = t;
        }

        public override string ToString()
        {
            return "Nom : " + Nom + "\n"+
                "Prénom : "+ Prenom + "\n" +
                "Téléphone : "+ Telephone+"\n";
        }



       public bool CreationClient()
        {
            string request = "insert into client(nom, prenom, telephone) values (@nom, @prenom, @telephone)";

            MySqlCommand command = new MySqlCommand(request, Connection.Instance);
            command.Parameters.Add(new MySqlParameter("@nom", Nom));
            command.Parameters.Add(new MySqlParameter("@prenom", Prenom));
            command.Parameters.Add(new MySqlParameter("@telephone", Telephone));
            Connection.Instance.Open();
            bool reponse = false;
            long idClient = 0;

            if (command.ExecuteNonQuery()>0)
            {

                idClient = command.LastInsertedId;
                Id = idClient;
            }



            command.Dispose();
            Connection.Instance.Close();

            if (idClient > 0)
            {
               reponse = true;

            }

            return reponse;
        }



        public static Client GetClientById(long idClient)
        {

            Connection.Instance.Close();
            Client client = null;

          
            string request = "select * from Client where id = @idClient";
            MySqlCommand command = new MySqlCommand(request, Connection.Instance);

            command.Parameters.Add(new MySqlParameter("@idClient", idClient));

            Connection.Instance.Open();

            MySqlDataReader reader = command.ExecuteReader();


            if (reader.Read())
            {


                client = new Client(reader.GetString(1), reader.GetString(2), reader.GetString(3))
                {
                    Id = reader.GetInt32(0)
                };


            }

            reader.Close();
            command.Dispose();
            Connection.Instance.Close();

            return client;

        }


    }
}
