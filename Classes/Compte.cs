﻿using System;
using System.Collections.Generic;
using System.Text;
using MySql.Data.MySqlClient;

namespace GestionCompteBancaire.Classes
{
    class Compte
    {
        private long id;

        private long idClient;

        protected decimal solde;

        private List<Operation> operations;




        public decimal Solde { get => solde; set => solde = value; }
     
        public List<Operation> Operations { get => operations; set => operations = value; }
        public long Id { get => id; set => id = value; }
        public long IdClient  { get => idClient; set => idClient = value; }

        public Compte( long idClient, decimal s = 0 )
        {
            IdClient = idClient;
            Solde = s;
            Operations = new List<Operation>();
        }


        public Compte()
        {

        }

        public bool CreationCompte()
        {

            string request = "insert into Compte(idClient, solde) values (@idClient, @solde)";

            MySqlCommand command = new MySqlCommand(request, Connection.Instance);

            command.Parameters.Add(new MySqlParameter("@idClient", IdClient));
            command.Parameters.Add(new MySqlParameter("@solde", Solde));
            Connection.Instance.Open();
            bool reponse = false;
            long idCompte = 0;

            if (command.ExecuteNonQuery() > 0)
            {
                idCompte = command.LastInsertedId;
                Console.WriteLine("Compte crée");
                Id = idCompte;
                reponse = true;
            }

            if (reponse)
            {
                Operations.ForEach(o => {
                    o.IdCompte = Id;
                    //if(!e.Save())
                    //{
                    //    retour = false;
                    //}
                    o.creeUneOperation();
                });
            }

            Operations = Operation.getlistOperations(Id);

            return reponse;
        }



        public virtual bool Depot(Operation o)
        {

            if(o.Montant <= 0)
            {
                return false;
            }
         
            Solde += o.Montant;

            string request = "update Compte Set solde=@solde where id=@id";

            MySqlCommand command = new MySqlCommand(request, Connection.Instance);

            command.Parameters.Add(new MySqlParameter("@solde", Solde));
            command.Parameters.Add(new MySqlParameter("@id", o.IdCompte));
            Connection.Instance.Open();
            int nbRow = command.ExecuteNonQuery();
            command.Dispose();

            Connection.Instance.Close();

            return nbRow == 1;
        }

        public virtual bool Retrait(Operation o)
        {
            if (Math.Abs(o.Montant) <= Solde)
            {
                Solde += o.Montant;
               
                string request = " update Compte Set solde=@solde where id=@id";

                MySqlCommand command = new MySqlCommand(request, Connection.Instance);

                command.Parameters.Add(new MySqlParameter("@solde", Solde));
                command.Parameters.Add(new MySqlParameter("@id", o.IdCompte));
                Connection.Instance.Open();
                int nbRow = command.ExecuteNonQuery();
                command.Dispose();

                Connection.Instance.Close();

                return nbRow == 1;
            }
         
            return false;
        }

        public override string ToString()
        {
            string retour = "Numero compte : "+ Id +"\n" +
                "Client : "+ IdClient +"\n" +
                "Liste operations : \n";
            Operations = Operation.getlistOperations(Id);
            foreach(Operation o in Operations)
            {
                retour += o.ToString() + "\n";
            }

            retour += "Solde : " + Solde;
            return retour;
        }


        public static List<Compte> GetComptes(int idClient)
        {
            Connection.Instance.Close();

            List<Compte> comptes = new List<Compte>();

            string request = "select * from Compte where idClient=idClient";

            MySqlCommand command = new MySqlCommand(request, Connection.Instance);

            command.Parameters.Add(new MySqlParameter("@idClient", idClient));


            Connection.Instance.Open();


            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {

                Compte compte = new Compte(reader.GetInt32(1), reader.GetInt32(2))
                {
                    Id = reader.GetInt32(0)
                };

                comptes.Add(compte);
              
            }

            reader.Close();
            command.Dispose();
            Connection.Instance.Close();



            return comptes;

        }


        public static Compte GetCompteById(long idCompte)
        {
            Connection.Instance.Close();
            Compte compte = null;

            string request = "select * from Compte where id=@idCompte";


            MySqlCommand command = new MySqlCommand(request, Connection.Instance);

            command.Parameters.Add(new MySqlParameter("@idCompte", idCompte));
            Connection.Instance.Open();


            MySqlDataReader reader = command.ExecuteReader();

            if (reader.Read())
            {

                compte = new Compte(reader.GetInt32(2), reader.GetInt32(1))
                {

                    Id = reader.GetInt32(0)
                };
            }

            //if (compte != null)
            //{

            //    compte.operations = Operation.
            //}
            reader.Close();
            command.Dispose();
            Connection.Instance.Close();
            return compte;
        }
    }
}
