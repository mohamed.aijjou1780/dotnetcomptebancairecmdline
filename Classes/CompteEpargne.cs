﻿using System;
using System.Collections.Generic;
using System.Text;
using MySql.Data.MySqlClient;

namespace GestionCompteBancaire.Classes
{
    class CompteEpargne : Compte
    {
        private int taux;
        private long idCompte;



        public int Taux { get => Taux1; }
        public int Taux1 { get => taux; set => taux = value; }
        public long IdCompte { get => idCompte; set => idCompte = value; }

        public CompteEpargne(long c, decimal s, int t, long compte) : base(c, s)
        {
            Taux1 = t;
            IdCompte = compte;
        }


        public CompteEpargne(int t, long compte)
        {
            Taux1 = t;
            IdCompte = compte;
        }

        public bool CreationCompte1()
        {

            string request = "insert into CompteE (taux, idCompte) values (@taux, @idCompte)";

            MySqlCommand command = new MySqlCommand(request, Connection.Instance);

            command.Parameters.Add(new MySqlParameter("@taux", Taux));
            command.Parameters.Add(new MySqlParameter("@idCompte", IdCompte));
           
            Connection.Instance.Open();

            bool reponse = false;
            long idCompteE = 0;

            if (command.ExecuteNonQuery() > 0)
            {
                idCompteE = command.LastInsertedId;
                Id = idCompteE;
                Console.WriteLine("Compte Epargne crée");
                reponse = true;
            }

            return reponse;
        }




        public void UpdateSolde()
        {


           Compte compte = Compte.GetCompteById(IdCompte);
            solde = compte.Solde;
            idCompte = compte.IdClient;

            solde += solde * Taux1 / 100;


            string request = "update CompteE set solde = @solde where idClient = @idClient";

            MySqlCommand command = new MySqlCommand(request, Connection.Instance);

            command.Parameters.Add(new MySqlParameter("@solde", solde));
            command.Parameters.Add(new MySqlParameter("@idClient", idCompte));

            Connection.Instance.Open();
            command.Dispose();
            Connection.Instance.Close();


        }

        public static Compte GetCompteById2(long idCompte)
        {
            Connection.Instance.Close();
            Compte compte = null;

            string request = "select * from CompteE where id=@idCompte";


            MySqlCommand command = new MySqlCommand(request, Connection.Instance);

            command.Parameters.Add(new MySqlParameter("@idCompte", idCompte));
            Connection.Instance.Open();


            MySqlDataReader reader = command.ExecuteReader();

            if (reader.Read())
            {

                compte = new CompteEpargne(reader.GetInt32(1), reader.GetInt64(2))
                {

                    Id = reader.GetInt32(0)
                };
            }

            //if (compte != null)
            //{

            //    compte.operations = Operation.
            //}
            reader.Close();
            command.Dispose();
            Connection.Instance.Close();
            return compte;
        }
    }
}
