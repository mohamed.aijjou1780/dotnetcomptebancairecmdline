﻿using System;
using System.Collections.Generic;
using System.Text;
using MySql.Data.MySqlClient;

namespace GestionCompteBancaire.Classes
{
    class ComptePayant : Compte
    {
        private int coutOperation;
        private long idCompte;
      

        public int CoutOperation { get => coutOperation; set => coutOperation = value; }
        public long IdCompte { get => idCompte; set => idCompte = value; }

        public ComptePayant(long idClient, long idC, int cOperation = 2, decimal s = 0) : base(idClient,s)
        {
            coutOperation = cOperation;
            IdCompte = idC;
        }

        public bool CreationCompte2()
        {

            string request = "insert into CompteP (coutOp, idCompte) values (@cout, @idCompte)";

            MySqlCommand command = new MySqlCommand(request, Connection.Instance);

            command.Parameters.Add(new MySqlParameter("@cout", CoutOperation));
            command.Parameters.Add(new MySqlParameter("@idCompte", IdCompte));
     

            Connection.Instance.Open();

            bool reponse = false;
            long idCompteP = 0;

            if (command.ExecuteNonQuery() > 0)
            {
                idCompteP = command.LastInsertedId;
                Id = idCompteP;
                Console.WriteLine("Compte Payant crée");
                reponse = true;
            }

            Connection.Instance.Close();
            return reponse;
        }

        public override bool Depot(Operation o)
        {
            if(o.Montant >= CoutOperation)
            {
                if(base.Depot(o))
                {
                    Operation oRetrait = new Operation(CoutOperation * -1, o.IdCompte);
                    return base.Retrait(oRetrait);
                }
                return false;
            }
            return false;
        }

        public override bool Retrait(Operation o)
        {
            if(Math.Abs(o.Montant)+ CoutOperation <= Solde)
            {
                //Operation oRetrait = new Operation(CoutOperation * -1);
                //return base.Retrait(o) && base.Retrait(oRetrait);
                return base.Retrait(o) && base.Retrait(new Operation(CoutOperation * -1, o.IdCompte));
            }
            return false;
        }

        public static Compte GetCompteById(long idCompte)
        {
            Connection.Instance.Close();
            Compte compte = null;

            string request = "select * from CompteP where id=@idCompte";


            MySqlCommand command = new MySqlCommand(request, Connection.Instance);

            command.Parameters.Add(new MySqlParameter("@idCompte", idCompte));
            Connection.Instance.Open();


            MySqlDataReader reader = command.ExecuteReader();

            if (reader.Read())
            {

                compte = new ComptePayant(reader.GetInt32(2), reader.GetInt32(1))
                {

                    Id = reader.GetInt32(0)
                };
            }

            //if (compte != null)
            //{

            //    compte.operations = Operation.
            //}
            reader.Close();
            command.Dispose();
            Connection.Instance.Close();
            return compte;
        }
    }
}
