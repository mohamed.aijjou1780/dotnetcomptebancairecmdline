﻿using System;
using MySql.Data.MySqlClient;

namespace GestionCompteBancaire.Classes
{
    public class Connection
    {
        private static MySqlConnection instance = null;


        public static MySqlConnection Instance
        {

            get
            {
                if(instance == null)
                {

                    instance = new MySqlConnection(@"server=localhost;user=root;password=;database=compteBancaireBdd");

                }

                return instance;


            }



        }

        public Connection()
        {
        }
    }
}
