﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace GestionCompteBancaire.Classes
{
    class IHM
    {
        private List<Compte> listeComptes;
        private int numeroCompte;
        private bool infos;

        public IHM()
        {
            infos = false;
            listeComptes = new List<Compte>();
        }

        public void Start()
        {
         
            string choix;
            Compte compte = null;
            string reponse;

            if (infos)
            {
                Console.Clear();
                Console.WriteLine("****** Connexion ******");
                Console.WriteLine("  ");
               
                compte = ActionRechercheCompte();


            }
            else
            {
                Console.Clear();
                Console.WriteLine("****** Bonjour ******");
                Console.WriteLine("  ");
                Console.WriteLine("Disposez-vous déjà d'un compte? (O/N)");
                reponse = Console.ReadLine();


                if (reponse == "O" || reponse == "o")
                {
                    compte = ActionRechercheCompte();

                }
                else if (reponse == "N" || reponse == "n")
                {
                    compte = null;
                }
            }

            do
            {

                if (compte != null)
                {
                    Console.Clear();
                    Console.WriteLine("*** Bienvenue Madame/Monsieur" + " " + Client.GetClientById(compte.IdClient).Nom + " " + "***");
                    Console.WriteLine("  ");
                    MenuPrincipal();
                    choix = Console.ReadLine();
                    switch (choix)
                    {
                        case "1":
                            ActionCreationCompte(compte);
                            break;
                        case "2":
                            ActionDepot(compte);
                            break;
                        case "3":
                            ActionRetrait(compte);
                            break;
                        case "4":
                            ActionAfficherOperationsEtSolde();
                            break;
                        case "5":
                            ActionCalculeInteretCompteEpargne();
                            break;
                        case "0":

                            //Quitter la console
                            Environment.Exit(0);
                            break;
                    }
                }
                else
                {
                    Console.Clear();
                    MenuPrincipal2();
                    choix = Console.ReadLine();
                    switch (choix)
                    {
                        case "1":
                            if (ActionCreationCompte(compte))
                            {
                                infos = true;
                                Start();
                            }
                            break;
                        case "0":
                            //Quitter la console
                            Environment.Exit(0);
                            break;
                    }

                }


            } while (choix != "0");
        }

        private void MenuPrincipal()
        {
            Console.WriteLine("-----Gestion compte-----");
            Console.WriteLine("1--Créer un compte");
            Console.WriteLine("2--Effectuer un dépot");
            Console.WriteLine("3--Effectuer un retrait");
            Console.WriteLine("4--Afficher un compte");
            Console.WriteLine("5--Calcule Interet compte epargne");
            Console.WriteLine("0--Quitter");
        }
        private void MenuPrincipal2()
        {
            Console.WriteLine("-----Gestion compte-----");
            Console.WriteLine("1--Créer un compte");
            Console.WriteLine("0--Quitter");
        }

        private void MenuCreationCompte()
        {
            Console.WriteLine("1--Compte standard");
            //Console.WriteLine("2--Compte epargne");
            //Console.WriteLine("3--Compte payant");
        }

        private void MenuCreationCompte2()
        {

            Console.WriteLine("1--Compte epargne");
            Console.WriteLine("2--Compte payant");
        }



        private void ActionAccesCompte(Compte compte)
        {

            Console.Clear();
            Console.WriteLine("---Identification du client");
            compte = ActionRechercheCompte();


        }



        private bool ActionCreationCompte(Compte compte)
        {
            bool reponse = false;
            if (compte == null)
            {
                Console.Clear();
                Console.WriteLine("---Information du client");
                Console.Write("Merci de saisir le nom : ");
                string nom = Console.ReadLine();
                Console.Write("Merci de saisir le prénom : ");
                string prenom = Console.ReadLine();
                Console.Write("Merci de saisir le téléphone : ");
                string telephone = Console.ReadLine();
                Client client = new Client(nom, prenom, telephone);
                if (client.CreationClient())
                {
                    Console.WriteLine("Le client est bien crée" + " " + client.Id);
                    reponse = true;
                }
                else
                {
                    Console.WriteLine("Erreur");
                    reponse = false;

                }
                Console.Write("Solde initial : ");
                string chaineSolde = Console.ReadLine();
                decimal solde = (chaineSolde == "") ? 0 : Convert.ToDecimal(chaineSolde);
                Console.WriteLine("---Type de compte : ");
                MenuCreationCompte();

                string choix = Console.ReadLine();
                switch (choix)
                {
                    case "1":
                        compte = new Compte(client.Id, solde);
                        if (compte.CreationCompte())
                        {
                            Console.WriteLine("Le Compte numero :" + " " + compte.Id + " " + "a bien été crée pour le client numero" + " " + client.Id);
                            reponse = true;
                        }
                        else
                        {
                            Console.WriteLine("Erreur");

                            reponse = false;
                        }


                        break;
                       
                }
                if (compte != null)
                {
                    Console.WriteLine("Compte crée avec le numéro : " + compte.Id);
                }
                else
                {
                    Console.WriteLine("Erreur création compte");
                }
                Console.Read();
            }
            else
            {
                Console.WriteLine("Votre compte principal actuel est le" + " " + compte.Id);
                Console.WriteLine(" ");
                Console.WriteLine("Merci d'indiquer quel compte souhaitez-vous créer?");
                MenuCreationCompte2();

                string choix = Console.ReadLine();
                switch (choix)
                {
                    case "1":
                        Console.Write("Merci de saisir le taux : ");
                        int taux = Convert.ToInt32(Console.ReadLine());
                        long idClient = compte.IdClient;
                        decimal solde = compte.Solde;
                        long idCompte = compte.Id;
                        Compte compte1 = new CompteEpargne(idClient, solde, taux, idCompte);
                        if (compte1 is CompteEpargne compteEpargne)
                        {
                            if (compteEpargne.CreationCompte1())
                            {
                                Console.WriteLine("Le Compte Epargne numero :" + " " + compte1.Id + " " + "a bien été crée pour le client numero" + " " + idClient);
                            }
                            else
                            {
                                Console.WriteLine("Erreur");

                            }

                        }
                        break;
                    case "2":
                        Console.WriteLine("Merci de saisir le cout de chaque opération : ");
                        int cout = Convert.ToInt32(Console.ReadLine());
                        long idClient2 = compte.IdClient;
                        decimal solde2 = compte.Solde;
                        long idCompte2 = compte.Id;
                        Compte compte2 = new ComptePayant(idClient2, idCompte2, cout, solde2 );
                        if (compte2 is ComptePayant comptePayant)
                            if (comptePayant.CreationCompte2())
                            {
                                Console.WriteLine("Le Compte Payant numero :" + " " + compte2.Id + " " + "a bien été crée pour le client numero" + " " + idClient2);
                            }
                            else
                            {
                                Console.WriteLine("Erreur");

                            }
                        break;
                }
                Console.Read();
            }

            return reponse;
        }
        private void ActionDepot(Compte compte)
        {
            Console.Clear();

            if (compte != null)
            {
                Console.Write("Le montant du dépot : ");
                decimal montant = Convert.ToDecimal(Console.ReadLine());
                Operation o = new Operation(montant, compte.Id);

                if (o.creeUneOperation())
                {
                    if (compte.Depot(o))
                    {
                        Console.WriteLine("Dépot éfféctué");
                        Console.WriteLine("Le nouveau solde : " + compte.Solde);
                    }
                    else
                    {
                        Console.WriteLine("Erreur dépot");
                    }

                }
                else
                {

                    Console.WriteLine("Operation a echoué");

                }
            }
            else
            {
                compte = ActionRechercheCompte();
                Console.Write("Le montant du dépot : ");
                decimal montant = Convert.ToDecimal(Console.ReadLine());
                Operation o = new Operation(montant, compte.Id);

                if (o.creeUneOperation())
                {
                    if (compte.Depot(o))
                    {
                        Console.WriteLine("Dépot éfféctué");
                        Console.WriteLine("Le nouveau solde : " + compte.Solde);
                    }
                    else
                    {
                        Console.WriteLine("Erreur dépot");
                    }

                }
                else
                {

                    Console.WriteLine("Operation a echoué");

                }

            }
            Console.Read();
        }

        private void ActionRetrait(Compte compte)
        {
            Console.Clear();

            if (compte != null)
            {
                Console.Write("Le montant du retrait : ");
                decimal montant = Convert.ToDecimal(Console.ReadLine());
                Operation o = new Operation(montant * -1, compte.Id);

                if (o.creeUneOperation())
                {
                    if (compte.Retrait(o))
                    {
                        Console.WriteLine("Retrait éfféctué");
                        Console.WriteLine("Le nouveau solde : " + compte.Solde);
                    }
                    else
                    {
                        Console.WriteLine("Erreur retrait");
                    }
                }
                else
                {

                    Console.WriteLine("L'operation a echoue");

                }
            }
            else
            {
                compte = ActionRechercheCompte();

                Console.Write("Le montant du retrait : ");
                decimal montant = Convert.ToDecimal(Console.ReadLine());
                Operation o = new Operation(montant * -1, compte.Id);

                if (o.creeUneOperation())
                {
                    if (compte.Retrait(o))
                    {
                        Console.WriteLine("Retrait éfféctué");
                        Console.WriteLine("Le nouveau solde : " + compte.Solde);
                    }
                    else
                    {
                        Console.WriteLine("Erreur retrait");
                    }
                }
                else
                {

                    Console.WriteLine("L'operation a echoue");

                }
            }
            Console.Read();
        }
        private void ActionAfficherOperationsEtSolde()
        {
            Console.Clear();
            Compte compte = ActionRechercheCompte();
            if (compte != null)
            {
                Console.WriteLine(compte);
            }
            else
            {
                Console.WriteLine("Aucun compte avec ce numéro");
            }
            Console.Read();
        }

        private void ActionCalculeInteretCompteEpargne()
        {
            Console.Clear();
            Compte compte = ActionRechercheCompteE();
            if (compte != null)
            {
                if (compte is CompteEpargne compteEpargne)
                {
                    compteEpargne.UpdateSolde();
                    Console.WriteLine("Le nouveau solde est de : " + compte.Solde);
                }
                else
                {
                    Console.WriteLine("Ce compte n'est pas un compte epargne");
                }
            }
            else
            {
                Console.WriteLine("Aucun compte avec ce numéro");
            }
            Console.Read();
        }

        private Compte ActionRechercheCompte()
        {

            Compte compte = null;
            Console.Write("Merci d'indiquer le numéro de compte : ");


            long numero = Convert.ToInt64(Console.ReadLine());

            compte = Compte.GetCompteById(numero);
            return compte;
        }

        private Compte ActionRechercheCompteE()
        {

            Compte compte = null;
            Console.Write("Merci d'indiquer le numéro de compte Epargne : ");


            long numero = Convert.ToInt64(Console.ReadLine());

            compte = CompteEpargne.GetCompteById2(numero);
            return compte;
        }
        private Compte ActionRechercheCompteP()
        {

            Compte compte = null;
            Console.Write("Merci d'indiquer le numéro de compte Payant : ");


            long numero = Convert.ToInt64(Console.ReadLine());

            compte = Compte.GetCompteById(numero);
            return compte;
        }


        private Client ActionRechercheClient(Compte compte)
        {
            Client client = null;
            long idClient = compte.IdClient;

            client = Client.GetClientById(idClient);

            return client;

        }
    }
}
