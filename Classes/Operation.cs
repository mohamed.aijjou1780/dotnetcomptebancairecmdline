﻿using System;
using System.Collections.Generic;
using System.Text;
using MySql.Data.MySqlClient;

namespace GestionCompteBancaire.Classes
{
    class Operation
    {
        private long id;
        private decimal montant;
        private long idCompte;

        public long Id { get => id; set => id = value; }
        public decimal Montant { get => montant; }
        public long IdCompte { get => idCompte; set => idCompte = value; }

        private Operation()
        {
         
        }

        public Operation(decimal m, long idCompte) : this()
        {
            montant = m;
            IdCompte = idCompte;
        }

        public override string ToString()
        {
            return "Numéro opération : " + Id + "\n" +
                "Montant opération : "+ Montant;
        }


       public bool creeUneOperation()
        {

            bool reponse = false;
            string request = "insert into Operation(idCompte, montant) values (@idCompte, @montant)";

            MySqlCommand command = new MySqlCommand(request, Connection.Instance);

            command.Parameters.Add(new MySqlParameter("@montant", Montant));
            command.Parameters.Add(new MySqlParameter("@idCompte", IdCompte));
            Connection.Instance.Open();
            Id = command.LastInsertedId;


            long idOperation = 0;

            if (command.ExecuteNonQuery() > 0)
            {

                idOperation= command.LastInsertedId;
                Id = idOperation;
            }

            command.Dispose();
            Connection.Instance.Close();

            if(Id> 0)
            {
                reponse = true;
            }

            return reponse;

        }




        public static List<Operation> getlistOperations(long numCompte)
        {
            Connection.Instance.Close();

            List<Operation> operations = new List<Operation>();

            string request = "select * from Operation where idCompte = @idCompte";


            MySqlCommand command = new MySqlCommand(request, Connection.Instance);

            command.Parameters.Add(new MySqlParameter("@idCompte", numCompte));

            Connection.Instance.Open();

            MySqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {

                Operation operation = new Operation(reader.GetInt32(1), reader.GetInt32(2))
                {
                    Id = reader.GetInt32(0)
                };

                operations.Add(operation);
            }


            command.Dispose();
            Connection.Instance.Close();
            return operations;

        }
    }
}
